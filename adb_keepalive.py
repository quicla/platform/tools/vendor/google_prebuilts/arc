#!/usr/bin/python3
#
# Copyright (C) 2017 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import argparse
import collections
import logging
import shutil
import subprocess
import sys
import time


_CONNECT_POLL_INTERVAL_SECONDS = 1
_KEEPALIVE_POLL_INTERVAL_SECONDS = 10


class Result(object):
  """An object that holds the result of a subprocess invocation."""

  def __init__(self, stdout, stderr, result):
    self.stdout = stdout
    self.stderr = stderr
    self.result = result


class Adb(object):
  """Manages connections with devices through adb."""

  def __init__(self, adb_binary):
    self._adb_binary = adb_binary

  def _run(self, *args):
    """Runs a single adb command."""
    p = subprocess.Popen([self._adb_binary] + list(args),
                         stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                         universal_newlines=True)
    stdout, stderr = p.communicate()
    result = p.wait()
    logging.debug('adb %s[%s]: %s', ' '.join(args), result, stdout.rstrip())
    return Result(stdout, stderr, result)

  def restart(self):
    """Restarts the adb server."""
    self._run('kill-server')
    self._run('start-server')

  def connect(self, device_serial):
    """Connects to a single device, and waits until it's ready."""
    logging.info('connecting to %s...', device_serial)
    while True:
      self._run('connect', device_serial)
      status = self.devices()
      if status[device_serial] == 'device' and self.ping(device_serial):
        break
      time.sleep(_CONNECT_POLL_INTERVAL_SECONDS)
    logging.info('connected to %s', device_serial)

  def devices(self):
    """Returns a mapping of all known devices and their status.

    The status string can be one of 'offline', 'bootloader', 'device', 'host',
    'recovery', 'sideload', 'unauthorized', or 'unknown'. 'device' means that
    the device is connected."""
    result = collections.defaultdict(lambda: 'unknown')
    for line in self._run('devices').stdout.strip().split('\n')[1:]:
      tokens = line.strip().split()
      if len(tokens) != 2:
        continue
      device, status = tokens
      result[device] = status
    return result

  def ping(self, device_serial):
    """Runs a no-op command over adb shell."""
    # |adb get-state| is not used since it can sometimes display "device" for
    # devices that are not actually ready to receive shell commands.
    return self._run('-s', device_serial, 'shell', 'exit').result == 0


def _parse_dut(dut):
  """Parses a device name.

  This adds the default port 22 if one was not explicitly added."""
  if ':' not in dut:
    default_port = 22
    return '%s:%d' % (dut, default_port)
  return dut


def main():
  parser = argparse.ArgumentParser(
      description=('A wrapper around adb connect that handles automatic '
                   're-connections in case the connection is lost'))
  parser.add_argument('--verbose', action='store_true')
  parser.add_argument('--interval', default=_KEEPALIVE_POLL_INTERVAL_SECONDS,
                      help='The interval (in seconds) to poll for devices.')
  parser.add_argument('--adb-binary', type=str, default=shutil.which('adb'),
                      help='The path of the adb binary')
  parser.add_argument('dut', nargs='+', type=_parse_dut,
                      help='The ip address of the devices')
  args = parser.parse_args()

  if args.verbose:
    logging.basicConfig(level=logging.DEBUG)
  else:
    logging.basicConfig(level=logging.INFO)

  if not args.adb_binary:
    logging.error('No default adb binary found. Provide one with --adb-binary')
    sys.exit(1)

  adb = Adb(args.adb_binary)
  adb.restart()
  for dut in args.dut:
    adb.connect(dut)
  logging.info('all DUTs connected')
  while True:
    try:
      time.sleep(args.interval)
      devices = adb.devices()
      for dut in args.dut:
        if devices[dut] != 'device':
          logging.warning('connection to %s lost. reconnecting', dut)
          adb.connect(dut)
    except KeyboardInterrupt:
      logging.info('shutting down')
      break


if __name__ == '__main__':
  main()
