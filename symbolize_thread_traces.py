#!/usr/bin/python3

# Copyright (C) 2017 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Attempts to get symbol information and display the source code for a
stack trace recorded for an Android thread traces log. """

from __future__ import print_function

import argparse
import logging
import os
import re
import sys

import lib.fingerprint
import lib.source_context_printer


EPILOG = """
Usage:
    %(prog)s path/to/traces.log
    %(prog)s -  # To read snippet from stdin
""".strip()

DEFAULT_CONTEXT_LINES = 5


def symbolify_traces(log_content, context_lines):
  fingerprint = lib.fingerprint.get_fingerprint_from_log_content(log_content)
  if not fingerprint:
    sys.exit('Did not find a build fingerprint in the output.')

  logging.info(
      'Found ARC build %s variant %s abi %s', fingerprint.build_id,
      fingerprint.variant, fingerprint.abi)

  symbol_start_path = fingerprint.get_symbol_start_path()
  if fingerprint.is_local_build:
    if not os.path.exists(symbol_start_path):
      sys.exit('Please run this again from your root Android build directory. '
               'Unable to locate locally built symbols.')
    logging.info('Using local build symbols from %s', symbol_start_path)

  source_context = lib.source_context_printer.SourceContextPrinter(
      fingerprint.get_remote_manifest(), context_lines=context_lines)

  for line in log_content.splitlines():
    print(line)

    m = re.search(r' pc ([0-9a-fA-F]+)  (.*)$', line)
    if not m:
      continue

    hex_addr = m.group(1)
    location = m.group(2)

    # Check to see if the location looks like a file path
    m = re.match(r'/([^ ]*)', location)
    if not m:
      continue

    # Convert it to a path referencing the downloaded symbols
    symbol_source_path = os.path.join(symbol_start_path, m.group(1))

    # Attempt to print more information about this location
    source_context.print_symbols_and_context_for_address(
        symbol_source_path, hex_addr)


def main():
  parser = argparse.ArgumentParser(
      description=__doc__, epilog=EPILOG,
      formatter_class=argparse.RawTextHelpFormatter)

  parser.add_argument(
      'log_output', type=argparse.FileType(),
      help='log file to parse, or - for stdin')

  parser.add_argument(
      '-C', '--context', dest='context_lines', default=DEFAULT_CONTEXT_LINES,
      help='lines of context to print')

  parser.add_argument(
      '-v', '--verbose', action='count', default=0,
      help='(repeatable) increase verbosity')

  args = parser.parse_args()
  logging.basicConfig(
      format='%(levelname)s: %(message)s',
      level=(logging.INFO, logging.DEBUG)[args.verbose])

  symbolify_traces(args.log_output.read(), args.context_lines)

if __name__ == '__main__':
  main()
