#!/usr/bin/python2
#
# Copyright (C) 2016 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""commit_origin_check verifies that all commits are cherry-picked.

Once a branch is considered to not be in active development, this script can be
used to verify that all commits that are pushed to it either have the 'DO NOT
MERGE' tag in the commit summary, or originate from the current development
branch.
"""

from __future__ import print_function

import argparse
import os
import re
import subprocess
import sys


CHERRY_PICK_RE = re.compile(r'^\(cherry picked from commit ([0-9a-f]{40})\)$')


def _parse_args():
  parser = argparse.ArgumentParser(
      formatter_class=argparse.RawDescriptionHelpFormatter,
      description='Checks that commits originate on the correct branch')
  parser.add_argument(
      '--source-branch', required=True, help='The source branch')
  parser.add_argument(
      '--target-branch', required=True,
      help=('The target branch. If a project is not tracking the target '
            'branch, then all commits will be ignored.'))
  parser.add_argument(
      'commit_message', metavar='commit-message', help='The commit message')

  return parser.parse_args()


def _branch_contains_commit(branch, commit):
  merge_base = subprocess.check_output(['/usr/bin/git', 'merge-base', branch,
                                        commit]).strip()
  return merge_base == commit


def main():
  args = _parse_args()
  if os.environ['REPO_RREV'] != args.target_branch:
    # This commit won't land on the target branch, so we'll ignore it.
    return

  lines = args.commit_message.split('\n')
  if 'DO NOT MERGE' in lines[0]:
    # This change is intended to not be merged, so it's branch-specific.
    # Ignore as well.
    return

  # Before we check that the commit is present in another branch, we need to
  # fetch the branch.
  subprocess.check_call(['/usr/bin/git', 'fetch', '-q',
                         os.environ['REPO_REMOTE'], args.source_branch])

  remote_branch = '%s/%s' % (os.environ['REPO_REMOTE'], args.source_branch)

  for line in lines[2:]:
    m = CHERRY_PICK_RE.match(line)
    if m and _branch_contains_commit(remote_branch, m.group(1)):
      break
  else:
    print('This branch is not a development branch.')
    print()
    print('All commits must either:')
    print('* Contain "DO NOT MERGE" in the commit summary')
    print('  if they are specific to this branch.')
    print('* Be cherry picked from %s' % remote_branch)
    print('  (did you remember to use `git cherry-pick -x`?)')
    sys.exit(1)


if __name__ == '__main__':
  main()
