# Updating push_to_device.py

push_to_device.py is a script to update Android image on Chrome OS for
development. It is used in other places too and has multiple copies and they
need auto be updated.

## board_specific_setup.sh

Usually changes to push_to_device.py that affects development environment needs
to be reflected for official builds.

See
https://chrome-internal.googlesource.com/chromeos/overlays/project-cheets-private/+/refs/heads/master/scripts/board_specific_setup.sh

## Android presubmit test.

see
https://chromium.googlesource.com/chromiumos/third_party/autotest/+/master/server/site_tests/provision_CheetsUpdate/push_to_device.py

## x20

There is http://x20/teams/arc++/push_to_device.py To update, you can copy to
`/google/data/rw/teams/arc++/`.

Other teams are using read only view `/google/data/ro/teams/arc++/` to update
Android image on Chromebooks.

## google3/third_party copy.

http://depot/google3/third_party/java/android_libs/arc/tools/push_to_device.py

See
https://g3doc.corp.google.com/third_party/java/android_libs/arc/tools/README.md
on how to update.
