#!/usr/bin/python3

# Copyright (C) 2016 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Displays a minidump stacktrace with symbols for a native ARC++ crash.

This helper script will download the right symbol file for the version of ARC++
referenced by the symbol file.  It will autodetect the ARC version and
architecture from the dump file.

It will then convert the downloaded symbols into breakpad format, for use by
minidump_stackwalk, which it will then invoke with the path to the symbols.
"""

import argparse
import logging

import lib.build_artifact_fetcher
import lib.crash_dump_server
import lib.minidump
import lib.symbol_file


EPILOG = """
The path to the minidump can be either to a locally downloaded file, or it can
be the URL of a crash report.

    %(prog)s path/to/minidump.dmp
    %(prog)s "https://crash.corp.google.com/1338f24100000000"
""".strip()


def main():
  parser = argparse.ArgumentParser(
      description=__doc__, epilog=EPILOG,
      formatter_class=argparse.RawTextHelpFormatter)

  parser.add_argument(
      'minidump_path', metavar='<mindump>',
      help='Path to the minidump file, or the crash report URL')
  parser.add_argument(
      '-v', '--verbose', action='count', default=0, help='Shows more messages.')

  args = parser.parse_args()
  logging.basicConfig(
      format='%(levelname)s: %(message)s',
      level=(logging.INFO, logging.DEBUG)[args.verbose])

  if lib.crash_dump_server.is_crash_server_url(args.minidump_path):
    crash_id = lib.crash_dump_server.get_report_id(args.minidump_path)
    minidump_path = lib.crash_dump_server.download_minidump(crash_id)
    minidump = lib.minidump.MinidumpFile(minidump_path)
  else:
    minidump = lib.minidump.MinidumpFile(args.minidump_path)

  assert minidump.arc_build_id, 'No ARC build ID found in the minidump.'

  logging.info(
      'Found ARC build %s arch %s', minidump.arc_build_id, minidump.arch)

  artifact_fetcher = lib.build_artifact_fetcher.BuildArtifactFetcher(
      minidump.arch, 'user', minidump.arc_build_id)
  symbols = lib.symbol_file.RemoteSymbolFile(artifact_fetcher)
  symbols.create_breakpad_symbols(minidump.modules)

  minidump.stacktrace_with_symbols(symbols.breakpad_symbol_path)

if __name__ == '__main__':
  main()
