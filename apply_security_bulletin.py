#!/usr/bin/python3

# Copyright 2017 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Script that applies security patches from a bulletin.

This should be run from a source checkout that has git branches tracking
the intended remote target branch for the patches. One can get a source
checkout into this state using a command like:
`repo forall -c 'git checkout NOV_2017_SECURITY goog/nyc-mr1-arc'`
"""

from __future__ import print_function

import argparse
import logging
import os
import subprocess
import zipfile

import lib.manifest_file

def main():
  parser = argparse.ArgumentParser(
      description=__doc__,
      formatter_class=argparse.RawTextHelpFormatter)
  parser.add_argument(
      '-v', '--verbose', action='store_true', help='Shows more messages.')
  parser.add_argument(
      '--release', default='android-7.1.1_r4', help='Android release')
  parser.add_argument(
      '--reviewers',
      default='lhchavez@google.com,yusukes@google.com,bhthompson@google.com',
      help='Reviewers for patches')
  parser.add_argument(
      '--manifest-path', default='.repo/manifests/default.xml',
      help='Path for the repo manifest file')
  parser.add_argument('zipfile', metavar='<bulletin-zip>',
                      type=argparse.FileType('rb'))

  args = parser.parse_args()

  logging.basicConfig(
      format='%(levelname)s: %(message)s',
      level=(logging.INFO, logging.DEBUG)[args.verbose])

  manifest = lib.manifest_file.ManifestFile(args.manifest_path)
  manifest.parse_manifest()
  projects = manifest.projects

  skipped = set()
  applied = set()
  with zipfile.ZipFile(args.zipfile, 'r') as zf:
    for name in sorted(zf.namelist()):
      # The zip file has the patches that are to be applied, a series of code
      # snippets (which are the original patches by third-party developers
      # against AOSP), instructions, and metadata. For patch files, |name| has
      # the following pattern:
      #
      #     <bulletin-id>/patches/<release>/<project-name>/<patch-filename>
      #
      # So one example of a patch file would be:
      #
      #     bulletin_2017_10/patches/android-7.1.1_r4/platform/libcore/ \
      #       0001-Disable-File.getCanonicalPath-caches.bulletin.patch
      components = name.split(os.sep)
      patch_filename = components[-1]
      if (len(components) < 3 or components[1] != 'patches' or
          components[2] != args.release or
          not patch_filename.endswith('.patch')):
        continue
      project_name = os.sep.join(components[3:-1])
      project = next((p for p in projects if p.name == project_name), None)
      if not project:
        logging.info('Skipping %s because "%s" is not checked out in the '
                     'current branch', name, project_name)
        continue
      with zf.open(name, mode='r') as f:
        contents = f.read()
      if project.path in skipped:
        logging.info('Skipping %s because "%s" has one failed patch',
                     name, project_name)
        with open(os.path.join(project.path, patch_filename), 'wb') as f:
          f.write(contents)
        continue
      with subprocess.Popen(['/usr/bin/git', 'am'], cwd=project.path,
                            stdin=subprocess.PIPE) as p:
        p.communicate(contents)
        returncode = p.wait()
      if returncode != 0:
        logging.error('Refusing to continue with %s', name)
        with open(os.path.join(project.path, patch_filename), 'wb') as f:
          f.write(contents)
        skipped.add(project.path)
        continue
      subprocess.check_call(['/usr/bin/git', 'filter-branch', '--force',
                             '--msg-filter', 'sed "0,/^/ s//DO NOT MERGE: /"',
                             'HEAD^..HEAD'], cwd=project.path)
      applied.add(project.path)

  logging.info('Uploading successful projects...')
  for project_path in applied - skipped:
    # -t instructs Gerrit to use the local branch name as a topic, so that all
    # patches land atomically. The 'yes\nyes\n' input is there because repo will
    # ask for confirmation to upload the patches. If there are more than ~5
    # patches in a branch, it will ask for a second confirmation, due to an
    # "unusual amount of patches being sent".
    cmd = ['repo', 'upload', '--current-branch', '.', '-t',
           '--reviewers=%s' % args.reviewers, '--no-verify']
    with subprocess.Popen(cmd, cwd=project_path) as p:
      p.communicate(b'yes\nyes\n')
      returncode = p.wait()
      if returncode:
        raise subprocess.CalledProcessError(returncode, cmd)

  logging.info('Successfully applied all patches in the following projects:')
  for project_path in applied - skipped:
    logging.info('\t%s', project_path)
  if skipped:
    logging.error('')
    logging.error('Failed to apply at least one patch in the following '
                  'projects:')
    for project_path in skipped:
      logging.error('\t%s', project_path)
    logging.error('Reviews for those projects were not sent, you need to do '
                  'that manually.')


if __name__ == '__main__':
  main()
