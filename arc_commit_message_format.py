#!/usr/bin/python3
#
# Copyright (C) 2017 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Script that checks that the commit message is correctly formatted."""

from __future__ import print_function

import logging
import os
import re
import sys
import argparse

# Regular expression for the "Topic" field
_TOPIC_FIELD_RE = re.compile(r'^Topic:\s*(.+)$', flags=re.MULTILINE)

# Regular expression for merge CLs
_ARC_MERGE_RE = re.compile(r'^ARC-Merge-From:\s*(.+)$', flags=re.MULTILINE)


def _parse_args():
  """Parses the arguments."""
  parser = argparse.ArgumentParser(description=__doc__)
  parser.add_argument('--loglevel', default='INFO',
                      choices=('DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'),
                      help='Logging level.')
  parser.add_argument('--topic-file', type=argparse.FileType('r'),
                      default=os.path.join(os.path.dirname(__file__),
                                           'ARC_TOPICS.txt'),
                      help=('The file path of valid topics.'))
  parser.add_argument(dest='commit_message', metavar='commit-message',
                      type=str, help='The commit message to validate.')
  args = parser.parse_args()
  logging.basicConfig(level=getattr(logging, args.loglevel))
  return args


def _is_merge_commit(commit_message):
  """Checks to see if the commit_message contains the magical merge string

  Args:
    commit_message: The commit message to validate.

  Returns:
    True or False.
  """
  return _ARC_MERGE_RE.search(commit_message) is not None


def _contains_valid_topic_fields(commit_message, topic_file):
  """Checks to see if the commit_message contains a valid topic field

  Args:
    commit_message: The commit message to validate.
    topic_file: The file object of valid topics

  Returns:
    True or False.
  """
  # Find the \"Topic:\" field
  topics = set([token.strip().lower() for line in _TOPIC_FIELD_RE.findall(
      commit_message) for token in line.split(',')])
  logging.debug('Topics from commit message: %s', topics)
  if not topics:
    print('Commit message is missing the "Topic:" field.')
    return False

  # Check if the topic exists in the file
  valid_topics = set(line.strip().lower() for line in topic_file if line)
  logging.debug('Topics from %s: %s', topic_file.name, valid_topics)
  difference = topics.difference(valid_topics)
  if difference:
    print('Invalid topic value(s): %s' % difference)
    print('Check %s for topic name' % topic_file.name)
    return False
  return True


def main():
  """The main entry."""
  args = _parse_args()

  # If this is a merge CL, skip all other checks
  logging.debug('Commit message: %s', args.commit_message)
  if _is_merge_commit(args.commit_message):
    logging.debug('Commit message is a merge CL')
    return 0

  if not _contains_valid_topic_fields(args.commit_message, args.topic_file):
    return 1
  return 0


if __name__ == '__main__':
  sys.exit(main())
