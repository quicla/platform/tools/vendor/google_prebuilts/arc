# Copyright (C) 2017 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from __future__ import print_function

import gdb
import gdb.printing

class ArtMirrorStringPrinter(object):
  """Print an art::mirror::String"""

  def __init__(self, val):
    self.val = val

  def to_string(self):
    ptr = self.val['value_'].reinterpret_cast(
        gdb.lookup_type('char16_t').pointer())
    length = self.val['count_']
    return ptr.string('utf-16', length=length)

  def display_hint(self):
    return 'string'

def build_art_printers():
  pp = gdb.printing.RegexpCollectionPrettyPrinter('art')
  pp.add_printer('art::mirror::String', '^art::mirror::String$',
                 ArtMirrorStringPrinter)
  return pp

gdb.printing.register_pretty_printer(
    gdb.current_objfile(),
    build_art_printers())
