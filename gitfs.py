#!/usr/bin/python3

# Copyright (C) 2016 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Provides a view of the repository at a particular Android build."""

from __future__ import print_function

import argparse
import errno
import logging
import os
import stat
import subprocess

from fuse import FUSE, FuseOSError, Operations

import lib.build_artifact_fetcher
import lib.crash_dump_server
import lib.manifest_file
import lib.util


class GitFS(Operations):
  """Implements a FUSE filesystem of an Android checkout for |manifest|."""

  def __init__(self, manifest):
    self._manifest = manifest
    self._root = {}
    self._uid = os.getuid()
    self._gid = os.getgid()
    self._handles = []
    self._fetched = {}
    for project in self._manifest.projects:
      try:
        entries = project.path.split(os.sep)
        curr = self._root
        for entry in entries[:-1]:
          if entry not in curr:
            curr[entry] = {}
          curr = curr[entry]
        curr[entries[-1]] = project
      except TypeError:
        # This only happens for GmsCorePrebuilt.
        pass

  def _split_path(self, partial):
    """
    Splits |partial| into project path, project info, and git-path.

    Project info will be a dictionary with the subdirectories if |partial| does
    not refer to a file within a project.

    Examples:

    build/envsetup.sh -> ('build', ProjectInfo(...), 'envsetup.sh')
    tools/vendor -> ('tools/vendor', {'google_prebuilts': ...}, '')
    """

    if os.path.isabs(partial):
      partial = partial[1:]
    if not partial:
      return (partial, self._root, None)
    entries = partial.split(os.sep)
    curr = self._root
    for idx, entry in enumerate(entries):
      if entry not in curr:
        raise FuseOSError(errno.ENOENT)
      curr = curr[entry]
      if isinstance(curr, lib.manifest_file.ProjectInfo):
        self._fetch(curr)
        return (curr.path, curr, os.sep.join(entries[idx+1:]))
    return (partial, curr, None)

  def _allocate_handle(self, buf):
    """Allocates the lowest possible file descriptor for |buf|."""

    for idx, h in enumerate(self._handles):
      if h is None:
        self._handles[idx] = buf
        return idx
    self._handles.append(buf)
    return len(self._handles) - 1

  def _check_handle(self, fh):
    """Asserts that |fh| is a valid file descriptor."""

    if fh >= len(self._handles) or self._handles[fh] is None:
      raise FuseOSError(errno.EBADFD)

  def _fetch(self, project):
    """Ensures that |project| has the necessary git files."""

    if project.path in self._fetched:
      return
    self._fetched[project.path] = True
    try:
      lib.util.check_call('/usr/bin/git', 'cat-file', '-e', project.revision,
                          cwd=project.path)
    except subprocess.CalledProcessError:
      lib.util.check_call('/usr/bin/git', 'fetch', 'goog',
                          self._manifest.branch_name, cwd=project.path)

  def access(self, path, mode):
    st = self.getattr(path)
    if mode & os.W_OK:
      raise FuseOSError(errno.EROFS)
    elif (mode & os.X_OK) and not st['st_mode'] & stat.S_IFDIR:
      raise FuseOSError(errno.EACCES)

  def getattr(self, path, _fh=None):
    project_path, project, git_path = self._split_path(path)
    if isinstance(project, dict):
      return {
          'st_atime': 0,
          'st_ctime': 0,
          'st_mtime': 0,
          'st_gid': self._gid,
          'st_uid': self._uid,
          'st_size': 4096,
          'st_mode': stat.S_IFDIR | stat.S_IREAD | stat.S_IEXEC,
          'st_nlink': 1,
      }
    elif isinstance(project, lib.manifest_file.ProjectInfo):
      try:
        cmd = ['/usr/bin/git', 'cat-file', '--batch-check', '--follow-symlinks']
        with subprocess.Popen(cmd, cwd=project_path, stdin=subprocess.PIPE,
                              stdout=subprocess.PIPE) as p:
          stdout, stderr = p.communicate(
              ('%s:./%s' % (project.revision, git_path)).encode('utf-8'))
          retcode = p.wait()
          if retcode != 0:
            raise subprocess.CalledProcessError(retcode, cmd, stderr)
          _, object_type, object_size = stdout.strip().split()
        if object_type == b'blob':
          file_mode = stat.S_IFREG | stat.S_IREAD
        elif object_type == b'tree':
          file_mode = stat.S_IFDIR | stat.S_IREAD | stat.S_IEXEC
        else:
          logging.error('Unknown git object type "%s"', object_type)
          raise FuseOSError(errno.ENOENT)
        return {
            'st_atime': 0,
            'st_ctime': 0,
            'st_mtime': 0,
            'st_gid': self._gid,
            'st_uid': self._uid,
            'st_size': int(object_size, 10),
            'st_mode': file_mode,
            'st_nlink': 1,
        }
      except subprocess.CalledProcessError:
        raise FuseOSError(errno.ENOENT)
    else:
      logging.error('Unknown object in tree "%r"', project)
      raise FuseOSError(errno.ENOENT)

  def readdir(self, path, _fh):
    project_path, project, git_path = self._split_path(path)

    dirents = ['.', '..']
    if isinstance(project, dict):
      dirents.extend(project.keys())
    elif isinstance(project, lib.manifest_file.ProjectInfo):
      try:
        entries = lib.util.check_output('/usr/bin/git', 'ls-tree',
                                        '--name-only', '%s:./%s' %
                                        (project.revision, git_path),
                                        cwd=project_path).strip()
        if entries:
          dirents += entries.split('\n')
      except subprocess.CalledProcessError:
        raise FuseOSError(errno.ENOENT)
    else:
      logging.error('Unknown object in tree "%r"', project)
      raise FuseOSError(errno.ENOENT)
    for r in dirents:
      yield r

  def readlink(self, path):
    self.getattr(path)
    raise FuseOSError(errno.EINVAL)

  def statfs(self, _path):
    raise FuseOSError(errno.ENOSYS)

  def chmod(self, path, _mode):
    self.getattr(path)
    raise FuseOSError(errno.EROFS)

  def chown(self, path, _uid, _gid):
    self.getattr(path)
    raise FuseOSError(errno.EROFS)

  def mknod(self, _path, _mode, _dev):
    raise FuseOSError(errno.EROFS)

  def rmdir(self, _path):
    raise FuseOSError(errno.EROFS)

  def mkdir(self, _path, _mode):
    raise FuseOSError(errno.EROFS)

  def unlink(self, _path):
    raise FuseOSError(errno.EROFS)

  def symlink(self, _name, _target):
    raise FuseOSError(errno.EROFS)

  def rename(self, _old, _new):
    raise FuseOSError(errno.EROFS)

  def link(self, _target, _name):
    raise FuseOSError(errno.EROFS)

  def utimens(self, _path, _times=None):
    raise FuseOSError(errno.EROFS)

  def open(self, path, _flags):
    project_path, project, git_path = self._split_path(path)
    if not isinstance(project, lib.manifest_file.ProjectInfo):
      raise FuseOSError(errno.ENOENT)
    try:
      contents = lib.util.check_output('/usr/bin/git', 'cat-file', 'blob',
                                       '%s:./%s' % (project.revision, git_path),
                                       cwd=project_path,
                                       universal_newlines=False)
      return self._allocate_handle(contents)
    except subprocess.CalledProcessError:
      raise FuseOSError(errno.ENOENT)

  def create(self, _path, _mode, _fi=None):
    raise FuseOSError(errno.EROFS)

  def read(self, _path, length, offset, fh):
    self._check_handle(fh)
    return self._handles[fh][offset:offset+length]

  def write(self, _path, _buf, _offset, fh):
    self._check_handle(fh)
    raise FuseOSError(errno.EINVAL)

  def truncate(self, _path, _length, fh=None):
    if fh is None:
      raise FuseOSError(errno.EROFS)
    self._check_handle(fh)
    raise FuseOSError(errno.EINVAL)

  def flush(self, _path, _fh):
    return 0

  def release(self, _path, fh):
    self._check_handle(fh)
    self._handles[fh] = None
    return 0

  def fsync(self, _path, _fdatasync, _fh):
    return 0


def main():
  parser = argparse.ArgumentParser(
      description=__doc__,
      formatter_class=argparse.RawTextHelpFormatter)

  parser.add_argument(
      'arc_build_id', metavar='<build-id>', help='ARC build ID')
  parser.add_argument(
      'mountpoint', metavar='<mountpoint>',
      help='Where to mount the filesystem')
  parser.add_argument(
      '-v', '--verbose', action='count', default=0, help='Shows more messages.')

  args = parser.parse_args()
  logging.basicConfig(
      format='%(levelname)s: %(message)s',
      level=(logging.INFO, logging.DEBUG)[args.verbose])

  # The architecture and build type don't matter.
  artifact_fetcher = lib.build_artifact_fetcher.BuildArtifactFetcher(
      'x86', 'user', args.arc_build_id)

  manifest = lib.manifest_file.RemoteManifestFile(artifact_fetcher)
  manifest.download_manifest()
  manifest.parse_manifest()

  FUSE(GitFS(manifest), args.mountpoint, nothreads=True, foreground=True)


if __name__ == '__main__':
  main()
