# Copyright (C) 2017 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""A helper class for working with the ARC target files that are mmap-ed."""

import io
import logging
import os
import re
import shutil
import tempfile

import lib.util as util


_DALVIK_CACHE_PATTERN = re.compile(
    r'^/data/dalvik-cache/(x86|x86_64|arm)/(.*)$')
_LOADABLE_EXTENSIONS = ['.oat', '.odex', '.art']


def is_elf(path):
  if not os.path.isfile(path):
    return False
  with open(path, 'rb') as f:
    sig = f.read(4)
    return sig == b'\x7FELF'


class TargetFile(object):
  def __init__(self, arch, target_file_path):
    self._arch = arch
    self._target_file_path = target_file_path

  @property
  def target_file_path(self):
    return self._target_file_path

  def create_elf_files(self):
    for root, _, files in os.walk(os.path.join(self.target_file_path,
                                               'system')):
      for filename in files:
        path = os.path.join(root, filename)
        if not any(path.endswith(ext) for ext in _LOADABLE_EXTENSIONS):
          continue
        elf_path = path + '.elf'
        if is_elf(path):
          # .odex and .oat files are already valid ELF files, so no further
          # processing is needed.
          continue
        if is_elf(elf_path):
          continue
        o_path = path + '.o'
        objcopy_args = []
        ld_args = []
        if self._arch == 'x86':
          objcopy_args.extend(['objcopy', '-I', 'binary', '-O', 'elf32-i386',
                               '-B', 'i386'])
          ld_args.extend(['ld', '-m', 'elf_i386'])
        elif self._arch == 'x86_64':
          # The binary architecture is still 'i386' for objcopy's purposes, even
          # under x86_64.
          objcopy_args.extend(['objcopy', '-I', 'binary', '-O', 'elf64-x86-64',
                               '-B', 'i386'])
          ld_args.extend(['ld', '-m', 'elf_x86_64'])
        elif self._arch == 'arm':
          objcopy_args.extend(['arm-linux-gnueabihf-objcopy', '-I', 'binary',
                               '-O', 'elf32-littlearm',
                               '-B', 'arm'])
          ld_args.extend(['arm-linux-gnueabihf-ld', '-m', 'armelf_linux_eabi'])
        else:
          assert False, 'Unsupported arch: %s' % self._arch
        objcopy_args.extend(['--rename-section',
                             '.data=.rodata,alloc,load,readonly,data,contents',
                             path, o_path])
        util.check_call(*objcopy_args)
        ld_args.extend(['-nostdlib', '-shared', '-soname', filename,
                        '-T', '/dev/stdin', '-o', elf_path, o_path])
        util.check_output(
            *ld_args,
            input='SECTIONS { .rodata : ALIGN(0x1000) { *(.rodata) } }')
        os.unlink(o_path)

  def loadable_path(self, android_path):
    assert os.path.isabs(android_path), (
        '"%s" must be an absolute path' % android_path)
    # Objects in the dalvik cache need special handling. We'll use the
    # unrelocated versions, but that's sufficient.
    m = _DALVIK_CACHE_PATTERN.match(android_path)
    if m:
      dirname, basename = os.path.split('/' + m.group(2).replace('@', '/'))
      android_path = os.path.join(dirname, m.group(1), basename)
    path = os.path.join(self.target_file_path, android_path[1:])
    if is_elf(path):
      return path
    elf_path = path + '.elf'
    if is_elf(elf_path):
      return elf_path
    return None


class RemoteTargetFile(TargetFile):
  def __init__(self, artifact_fetcher):
    self._artifact_fetcher = artifact_fetcher
    self._zipped_target_file_path = util.helper_temp_path(
        'cheets_%s-%s-target_files-%s.zip' % (
            self._artifact_fetcher.arch,
            self._artifact_fetcher.variant,
            self._artifact_fetcher.build_id))
    self._unzipped_symbol_path = (
        os.path.splitext(self._zipped_target_file_path)[0])

    super(RemoteTargetFile, self).__init__(self._artifact_fetcher.arch,
                                           self._unzipped_symbol_path)

  def download_target_files(self):
    if os.path.exists(self._zipped_target_file_path):
      return

    remote_path = 'cheets_%s-target_files-%s.zip' % (
        self._artifact_fetcher.arch, self._artifact_fetcher.build_id)
    self._artifact_fetcher.fetch(remote_path, self._zipped_target_file_path)

  def unzip_target_files(self):
    self.download_target_files()
    if os.path.exists(self.target_file_path):
      return

    logging.info('Extracting %s ...',
                 os.path.basename(self._zipped_target_file_path))

    # We do the unzip to a slightly different path in case there is a failure
    # that leaves us with a partially unzipped destination.
    target_staging = self._unzipped_symbol_path + '.tmp'
    if os.path.exists(target_staging):
      shutil.rmtree(target_staging)
    util.makedirs(target_staging)
    with tempfile.TemporaryDirectory(
        dir=os.path.dirname(self._unzipped_symbol_path)) as unzip_tmp:
      unzip_cmd = (['unzip', self._zipped_target_file_path, '-d', unzip_tmp] +
                   ['*%s' % ext for ext in _LOADABLE_EXTENSIONS])
      util.check_output(*unzip_cmd)
      for child in os.listdir(unzip_tmp):
        src = os.path.join(unzip_tmp, child)
        # The .zip contains subdirectories in all-caps. Rename them while
        # moving.
        dest = os.path.join(target_staging, child.lower())
        if not os.path.isdir(src):
          continue
        os.rename(src, dest)
    os.rename(target_staging, self._unzipped_symbol_path)


class LocalTargetFile(TargetFile):
  def __init__(self, arch):
    super(LocalTargetFile, self).__init__(
        arch, os.path.join('out', 'target', 'product', 'cheets_' + arch))
