# Copyright (C) 2017 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""A helper class for working with stored ARC build manifest files"""

import collections
import os.path
import xml.etree.cElementTree

import lib.util as util

ProjectInfo = collections.namedtuple(
    'ProjectInfo', ['name', 'path', 'revision', 'clone_depth', 'groups'])

class ManifestFile(object):
  """A class for processing repo manifest files."""

  def __init__(self, local_manifest_path):
    self._local_manifest_path = local_manifest_path

    self._manifest_content = None
    self._path_to_project_info = {}

  def parse_manifest(self):
    """Parses the read manifest file, if it hasn't been done yet."""
    if not self._manifest_content:
      self._manifest_content = xml.etree.ElementTree.parse(
          self._local_manifest_path)
      self._branch_name = self._manifest_content.find('default').get('revision')
      # In repo, 'path' is optional and 'name' is the fallback.
      self._path_to_project_info = dict(
          (project.get('path', project.get('name')),
           ProjectInfo(project.get('name'),
                       project.get('path'),
                       project.get('revision'),
                       project.get('clone-depth'),
                       project.get('groups')))
          for project in self._manifest_content.getroot().iter('project'))

  def find_project_for_path(self, path):
    """Given a relative path, finds the manifest Project entry for it."""
    self.parse_manifest()

    while path:
      project = self._path_to_project_info.get(path)
      if project is not None:
        return project
      path = os.path.dirname(path)
    return None

  @property
  def projects(self):
    """Gets all the projects from the manifest."""
    self.parse_manifest()

    return [project for _, project in
            sorted(self._path_to_project_info.items())]

  @property
  def branch_name(self):
    """Gets the branch name from the manifest."""
    self.parse_manifest()

    return self._branch_name


class RemoteManifestFile(ManifestFile):
  """A helper class for working with build server repo manifest files."""

  def __init__(self, fetcher):
    self._artifact_fetcher = fetcher
    self._remote_manifest_path = (
        'manifest_%s.xml' % self._artifact_fetcher.build_id)
    local_manifest_path = util.helper_temp_path(
        'manifest_%s.xml' % self._artifact_fetcher.build_id)

    super(RemoteManifestFile, self).__init__(local_manifest_path)

  def download_manifest(self):
    """Downloads the manifest with the fetcher, if it hasn't been done yet."""
    if os.path.exists(self._local_manifest_path):
      return
    self._artifact_fetcher.fetch(
        self._remote_manifest_path, self._local_manifest_path)
