# Copyright (C) 2017 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""A helper class for working with stored ARC build manifest files"""

from __future__ import print_function

import logging
import os
import re
import subprocess

# The useful output from addr2line should look like:
#     <source path>:<line num><extra info>
BUILD_MACHINE_SOURCE_FILE_AND_LINE_RE = r'/proc/self/cwd/([^:]+?):(\d+)(.*)'


class SourceContextPrinter(object):
  """Helps display a few lines of context from a git versioned source file."""

  def __init__(self, manifest, indent_normal=None, indent_highlight=None,
               context_lines=2):
    """Construct the helper class, using common version and output settings.

    Args:
        manifest: a manifest_file.ManifestFile which allows us to map a
            source code path to a version. If None, the HEAD version of each
            file will be used.
        indent_normal: the string to use when indenting  lines before and
            after a given line number.
        indent_highlight: is the string to use when indenting the specified
            line number to better highlight it in the output.
    """
    self._manifest = manifest
    self._indent_normal = indent_normal or '    '
    self._indent_highlight = indent_highlight or '>>>>'
    self._context_lines = context_lines

  def print_source_code_context(self, source_path, line_num):
    """Attempts to print an excerpt of |source_path| at |line_num|."""
    if self._manifest:
      project = self._manifest.find_project_for_path(source_path)
      if not project:
        logging.debug('Source path %s not found in manifest', source_path)
        return

      if not os.path.exists(project.path):
        logging.debug('Project path %s not found locally', project.path)
        return

      project_path = os.path.abspath(project.path)
      project_revision = project.revision
    else:
      project_path = subprocess.check_output([
          'git', '-C', os.path.dirname(source_path), 'rev-parse',
          '--show-toplevel'], universal_newlines=True).strip()
      project_revision = 'HEAD'

    try:
      # Get the complete source file at this version
      rel_source_path = os.path.relpath(
          os.path.abspath(source_path), project_path)
      source_code = subprocess.check_output(
          ['git', '-C', project_path, 'show', '%s:%s' % (project_revision,
                                                         rel_source_path)],
          universal_newlines=True)

      # Convert it into a list of lines
      source_code = source_code.splitlines()

      # Determine the range of line numbers to use for the context
      begin_line_num = max(line_num - self._context_lines, 1)
      end_line_num = min(line_num + self._context_lines, len(source_code))

      # Extract the context lines
      source_lines = source_code[begin_line_num - 1:end_line_num]

      # Print out the context lines, with line numbers, and highlighting the
      # line from the backtrace.
      for i, source_line in enumerate(source_lines):
        source_line_num = begin_line_num + i
        indent = (self._indent_highlight if source_line_num == line_num
                  else self._indent_normal)
        print('%s% 5d: %s' % (indent, source_line_num, source_line))

    except subprocess.CalledProcessError as e:
      logging.exception(e)
      print('Unable to get source code for %s %s. Maybe you need a repo sync?',
            project_revision, source_path)
      return

  def print_symbols_and_context_for_address(self, symbol_source_path,
                                            hex_address):
    """Prints out as much information as is available for an address"""
    if not os.path.exists(symbol_source_path):
      return

    # Get line number information from the symbol file for the indicated address
    output = subprocess.check_output(
        ['addr2line', '-e', symbol_source_path, hex_address],
        universal_newlines=True)
    # Check if the line looks like it starts with an absolute build file path
    m = re.match(BUILD_MACHINE_SOURCE_FILE_AND_LINE_RE, output)
    if not m:
      # Print it out since it still might be informative.
      print('%s%s' % (self._indent_normal, output))
      return

    # Extract the relative source path and the line number
    source_path = m.group(1)
    line_num = int(m.group(2))
    extra_context = m.group(3)

    # Print out the information with the relative path
    print('%s%s: %d%s' % (self._indent_normal, source_path, line_num,
                          extra_context))

    # Attempt to print out the source code for the version and line number
    self.print_source_code_context(source_path, line_num)
