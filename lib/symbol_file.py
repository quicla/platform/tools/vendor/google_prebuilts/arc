# Copyright (C) 2016 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""A helper class for working with the ARC debug symbols"""

import abc
import logging
import os
import shutil

import lib.util as util


class SymbolFile(object, metaclass=abc.ABCMeta):
  def __init__(self):
    pass

  @property
  @abc.abstractmethod
  def symbol_start_path(self):
    pass


class RemoteSymbolFile(SymbolFile):
  def __init__(self, artifact_fetcher):
    super(RemoteSymbolFile, self).__init__()
    self._artifact_fetcher = artifact_fetcher

    self._zipped_symbol_path = util.helper_temp_path(
        'cheets_%s-%s-symbols-%s.zip' % (
            self._artifact_fetcher.arch,
            self._artifact_fetcher.variant,
            self._artifact_fetcher.build_id))

    self._unzipped_symbol_path = os.path.splitext(self._zipped_symbol_path)[0]

    prefix = os.path.join(
        self._unzipped_symbol_path, 'out', 'target', 'product',
        'cheets_' + self._artifact_fetcher.arch, 'symbols', 'system')

    self._module_search_path = (
        os.path.join(prefix, 'bin'),
        os.path.join(prefix, 'lib'),
        os.path.join(prefix, 'lib', 'hw'))

    self._breakpad_symbol_path = os.path.join(
        self._unzipped_symbol_path, 'breakpad')

  def download_symbols(self):
    if os.path.exists(self._zipped_symbol_path):
      return

    remote_path = 'cheets_%s-symbols-%s.zip' % (
        self._artifact_fetcher.arch, self._artifact_fetcher.build_id)
    self._artifact_fetcher.fetch(remote_path, self._zipped_symbol_path)

  def unzip_symbols(self):
    self.download_symbols()
    if os.path.exists(self._unzipped_symbol_path):
      return

    logging.info('Extracting %s ...',
                 os.path.basename(self._zipped_symbol_path))

    # We do the unzip to a slightly different path in case there is a failure
    # that leaves us with a partially unzipped destination.
    unzip_tmp = self._unzipped_symbol_path + '.tmp'
    if os.path.exists(unzip_tmp):
      shutil.rmtree(self._unzipped_symbol_path + '.tmp')
    util.makedirs(os.path.dirname(unzip_tmp))
    util.check_output(
        'unzip',
        self._zipped_symbol_path,
        '-d', unzip_tmp)
    os.rename(unzip_tmp, self._unzipped_symbol_path)

  def _process_symbols(self, module, input_path, output_path):
    logging.debug('Dumping symbols from %s ...', input_path)
    dump = util.check_output(
        util.get_prebuilt('dump_syms'), input_path)

    info = dump.splitlines()[0].split()
    assert info[0] == 'MODULE'
    assert info[1] == 'Linux'
    assert info[2] == self._artifact_fetcher.arch
    assert info[3] == module.hash
    assert info[4] == module.name

    util.makedirs(os.path.dirname(output_path))
    logging.debug('Writing %s', output_path)
    with open(output_path, 'w') as symfile:
      symfile.write(dump)

  def _process_module(self, module):
    processed_module_path = os.path.join(
        self._breakpad_symbol_path, module.name, module.hash,
        module.name + '.sym')
    if os.path.exists(processed_module_path):
      return

    logging.debug('Processing module %s', module.name)
    for path in self._module_search_path:
      found_module_path = os.path.join(path, module.name)
      if os.path.exists(found_module_path):
        self._process_symbols(module, found_module_path, processed_module_path)
        return

    logging.warn('Module %s not found!', module)

  def create_breakpad_symbols(self, modules):
    logging.info('Converting symbols to breakpad format ...')
    self.unzip_symbols()
    for module in modules:
      self._process_module(module)

  @property
  def symbol_start_path(self):
    return os.path.join(
        self._unzipped_symbol_path, 'out', 'target', 'product',
        'cheets_' + self._artifact_fetcher.arch, 'symbols')

  @property
  def breakpad_symbol_path(self):
    return self._breakpad_symbol_path


class LocalSymbolFile(SymbolFile):
  def __init__(self, arch):
    super(LocalSymbolFile, self).__init__()
    self._arch = arch

  @property
  def symbol_start_path(self):
    return os.path.join(
        'out', 'target', 'product', 'cheets_' + self._arch, 'symbols')
