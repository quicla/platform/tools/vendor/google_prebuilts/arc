#!/usr/bin/python3

# Copyright (C) 2016 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Cleans the Android out/target/product/ directory for the current target of
files that do not seem to be built by ninja."""

from __future__ import print_function

import argparse
import logging
import os
import random
import re
import subprocess
import sys

_IGNORE_PRODUCT_DIRECTORY_PATHS = [
    # If certain files are deleted under these paths, the build will fail, as
    # they contain generated files listed as input, but not explicitly listed as
    # an output.
    'dex_bootjars/',
    'obj/STATIC_LIBRARIES/libmcldScript_intermediates/',
    'obj/STATIC_LIBRARIES/libstagefright_mediafilter_intermediates/',

    # This directory contains various linux commands for the target. If they
    # are removed, they won't be rebuilt automatically AND they won't be
    # available on the target subsequently.
    'system/bin/',

    # These paths just add many files that would be deleted and then rebuilt.
    # But they should otherwise rebuild cleanly.
    'obj/STATIC_LIBRARIES/libc_common_intermediates/',
    'obj/STATIC_LIBRARIES/libwebrtc_audio_coding_gnustl_static_intermediates/',
]

_IGNORE_PRODUCT_FILE_EXTENSIONS = ['.P']

_IGNORE_PRODUCT_FILE_PATHS = [
    # These files are currently built, and are not being rebuilt if deleted.
    'build_fingerprint.txt',
    'clean_steps.mk',
    'previous_build_config.mk',
    'root/charger',
    'root/etc',
    'root/sbin/ueventd',
    'root/sbin/watchdogd',
    'root/sdcard',
    'root/vendor',
    'symbols/system/framework/arm/boot.oat',
    'symbols/system/framework/x86/boot.oat',
    'system/app/Bluetooth/lib/arm/libbluetooth_jni.so',
    'system/app/Bluetooth/lib/x86/libbluetooth_jni.so',
    'system/app/Camera2/lib/arm/libjni_jpegutil.so',
    'system/app/Camera2/lib/arm/libjni_tinyplanet.so',
    'system/app/Camera2/lib/x86/libjni_jpegutil.so',
    'system/app/Camera2/lib/x86/libjni_tinyplanet.so',
    'system/app/PrintSpooler/lib/arm/libprintspooler_jni.so',
    'system/app/PrintSpooler/lib/x86/libprintspooler_jni.so',
    'system/framework/arm/boot.oat',
    'system/framework/x86/boot.oat',
    'system/lib/libGLESv3.so',
    'system/priv-app/DefaultContainerService/lib/arm/libdefcontainer_jni.so',
    'system/priv-app/DefaultContainerService/lib/x86/libdefcontainer_jni.so',
]


def _check_output(args):
  try:
    logging.info('Running: "%s"', ' '.join(args))
    return subprocess.check_output(args).decode()
  except subprocess.CalledProcessError as e:
    sys.exit('Failed to run "%s". %s', ' '.join(args), e)


def get_ninja_built_files(ninja_cmd):
  targets = _check_output([ninja_cmd, '-t', 'targets', 'all'])
  # The output is a series of lines that look like:
  #    path/to/foo.o: path/to/foo.c
  # Simply strip off everything after the first colon to get the list of built
  # files.
  # We check for a prefix of 'out/' as some targets are pseudo-targets (such as
  # "all" and 'clean")
  out_prefix = 'out' + os.path.sep
  for line in targets.splitlines():
    output = line.split(':', 1)[0]
    if output.startswith(out_prefix):
      yield output


def get_all_files_under(base_path):
  for root, _, files in os.walk(base_path):
    for path in files:
      yield os.path.join(root, path)


def gen_prefix_filter(prefix_list):
  prefix_re = re.compile(
      '^' + '|'.join(re.escape(prefix) for prefix in prefix_list))
  return prefix_re.search


def gen_suffix_filter(suffix_list):
  suffix_re = re.compile(
      '|'.join(re.escape(suffix) for suffix in suffix_list) + '$')
  return suffix_re.search


def gen_exact_filter(path_list):
  exact_re = re.compile(
      '^' + '|'.join(re.escape(path) for path in path_list) + '$')
  return exact_re.search


def clean_cheets_build(dry_run=False, verbose=False):
  target_product = os.getenv('TARGET_PRODUCT')
  if not target_product:
    sys.exit('No target product -- did you run lunch?')

  target_build_dir = os.path.join('out/target/product/', target_product)
  target_ninja_script = 'out/ninja-%s.sh' % target_product

  if not os.path.isdir(target_build_dir):
    sys.exit('Nothing to do -- "%s" does not exist' % target_build_dir)

  if not os.path.exists(target_ninja_script):
    sys.exit('Please run "m out/ninja-${TARGET_PRODUCT}.sh" to regenerate the '
             'ninja scripts.')

  built = set(get_ninja_built_files(target_ninja_script))

  match_path_prefix = gen_prefix_filter(
      [os.path.join(target_build_dir, prefix)
       for prefix in _IGNORE_PRODUCT_DIRECTORY_PATHS])

  match_path_suffix = gen_suffix_filter(_IGNORE_PRODUCT_FILE_EXTENSIONS)

  match_path_exact = gen_exact_filter(
      [os.path.join(target_build_dir, prefix)
       for prefix in _IGNORE_PRODUCT_FILE_PATHS])

  present = set(path for path in get_all_files_under(target_build_dir)
                if (not match_path_prefix(path) and
                    not match_path_suffix(path) and
                    not match_path_exact(path)))

  if logging.getLogger().isEnabledFor(logging.DEBUG):
    logging.debug('Sample of ten built files:')
    logging.debug('\n'.join(sorted(random.sample(built, 10))))
    logging.debug('Sample of ten present files:')
    logging.debug('\n'.join(sorted(random.sample(present, 10))))

  clean_list = present - built

  if not clean_list:
    print('Nothing to clean.')
    sys.exit()

  if dry_run:
    print(len(clean_list), 'files would be removed.')
    if len(clean_list) > 50 and not verbose:
      print('Here is a random sample (use -v to see all):')
      print('\n'.join(sorted(random.sample(clean_list, 50))))
    else:
      print('\n'.join(sorted(clean_list)))
    sys.exit()

  print('Removing', len(clean_list), 'files')
  for path in clean_list:
    assert not os.path.isabs(path)
    os.remove(path)


def main():
  parser = argparse.ArgumentParser(description=__doc__)
  parser.add_argument(
      '-v', '--verbose', action='count', default=0, help='Shows more messages.')
  parser.add_argument(
      '-n', '--dry-run', action='store_true',
      help='Lists the files that would be deleted instead of deleting them.')
  args = parser.parse_args()
  logging.basicConfig(
      format='%(levelname)s: %(message)s',
      level=(logging.WARNING, logging.INFO, logging.DEBUG)[args.verbose])

  clean_cheets_build(dry_run=args.dry_run, verbose=args.verbose)

if __name__ == '__main__':
  main()
