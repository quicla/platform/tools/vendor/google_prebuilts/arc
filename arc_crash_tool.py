#!/usr/bin/python3

# Copyright (C) 2017 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Tool to inspect native ARC++ crashes using gdb instead of breakpad.

This tool will download the right symbol file for the version of ARC++
referenced by the symbol file.  It will autodetect the ARC version and
architecture from the dump file.

It will then convert the minidump to gdb's core format and then will create a
symbol script that can be used with gdb, in order to print a stack trace or
start a debugging session with the core file.
"""

from __future__ import print_function

import argparse
import collections
import logging
import os
import os.path
import re
import subprocess
import sys
import tempfile

import lib.build_artifact_fetcher
import lib.crash_dump_server
import lib.manifest_file
import lib.minidump
import lib.symbol_file
import lib.target_file
import lib.util


EPILOG = """
The path to the minidump can be either to a locally downloaded file, or it can
be the URL of a crash report.

    %(prog)s path/to/minidump.dmp
    %(prog)s "https://crash.corp.google.com/1338f24100000000"
""".strip()
_MAP_RE = re.compile(
    b'.*?([0-9a-f]+)-[0-9a-f]+ ([r-][w-][x-][p-]) ([0-9a-f]+) '
    b'[0-9:]+ [0-9]+ (/.+)$')
_STACK_TRACE_RE = re.compile(
    r'^(#\d+)\s+(?:(0x[a-f0-9]+) in )?(.*) at ([a-zA-Z0-9_/.-]+):(\d+)$')
_KNOWN_EXECUTABLE_SECTION_NAMES = ['.text', '.plt']

# This is the list of all Java root directories that contain files that go in at
# least one frameworks*.jar.
_JAVA_DIRECTORIES = [
    'frameworks/base/core/java',
    'frameworks/base/drm/java',
    'frameworks/base/graphics/java',
    'frameworks/base/keystore/java',
    'frameworks/base/location/java',
    'frameworks/base/location/lib/java',
    'frameworks/base/media/java',
    'frameworks/base/media/lib/remotedisplay/java',
    'frameworks/base/media/lib/signer/java',
    'frameworks/base/media/lib/tvremote/java',
    'frameworks/base/media/mca/effect/java',
    'frameworks/base/media/mca/filterfw/java',
    'frameworks/base/media/mca/filterpacks/java',
    'frameworks/base/nfc-extras/java',
    'frameworks/base/opengl/java',
    'frameworks/base/rs/java',
    'frameworks/base/sax/java',
    'frameworks/base/services/accessibility/java',
    'frameworks/base/services/appwidget/java',
    'frameworks/base/services/backup/java',
    'frameworks/base/services/core/arc',
    'frameworks/base/services/core/java',
    'frameworks/base/services/devicepolicy/java',
    'frameworks/base/services/java',
    'frameworks/base/services/midi/java',
    'frameworks/base/services/net/java',
    'frameworks/base/services/print/arc',
    'frameworks/base/services/print/java',
    'frameworks/base/services/restrictions/java',
    'frameworks/base/services/retaildemo/java',
    'frameworks/base/services/usage/java',
    'frameworks/base/services/usb/java',
    'frameworks/base/services/voiceinteraction/java',
    'frameworks/base/telecomm/java',
    'frameworks/base/telephony/java',
    'frameworks/base/tools/aapt2/java',
    'frameworks/base/wifi/java',
    'libcore/libart/src/main/java',
    'libcore/luni/src/main/java',
    'libcore/ojluni/src/main/java',
]


LoadAddress = collections.namedtuple(
    'LoadAddress', ['base', 'permissions', 'offset'])


def _get_abort_message(minidump_path):
  """Gets the abort message that's saved in the stack."""
  x86_magic = b'\x2d\x5f\x57\xcb'
  arm_magic = b'\xcb\x57\x5f\x2d'

  with open(minidump_path, 'rb') as f:
    magic_buf = f.read(4)
    assert magic_buf == b'MDMP', '"%s" is not a minidump.' % minidump_path
    while True:
      c = f.read(1)
      if len(c) == 0:
        break
      magic_buf = magic_buf[1:] + c
      if magic_buf == x86_magic:
        length = ord(f.read(1)) | ord(f.read(1)) << 8
        return f.read(length).decode('utf-8')
      elif magic_buf == arm_magic:
        length = ord(f.read(1)) << 8 | ord(f.read(1))
        return f.read(length).decode('utf-8')

  return None


def _get_load_addresses(minidump_filename):
  """Gets the addresses of all the loaded libraries."""
  load_addresses = collections.defaultdict(list)
  with open(minidump_filename, 'rb') as f:
    for line in f:
      m = _MAP_RE.match(line)
      if not m:
        continue
      groups = m.groups()
      base = int(groups[0], 16)
      perms = groups[1]
      offset = int(groups[2], 16)
      binary = groups[3].decode('utf-8')
      load_addresses[binary].append(LoadAddress(base, perms, offset))
  return load_addresses


def _get_addresses(load_addresses, symbols, target_files):
  """Gets the addresses and offsets of all loaded libraries."""
  addresses = {}
  symbol_root = symbols.symbol_start_path
  for binary, address_list in load_addresses.items():
    path = os.path.join(symbol_root, binary[1:])
    if not os.path.isfile(path):
      path = target_files.loadable_path(binary)
    if not path or not os.path.isfile(path):
      continue
    assert lib.target_file.is_elf(path)
    objdump = subprocess.check_output(['/usr/bin/objdump', '-wh', path],
                                      universal_newlines=True)
    sections = {}
    exec_load_address = ([load_address for load_address in address_list
                          if b'x' in load_address.permissions] or [None])[0]
    if exec_load_address:
      for line in objdump.split('\n'):
        for section_name in _KNOWN_EXECUTABLE_SECTION_NAMES:
          fields = line.strip().split()
          if len(fields) > 6 and fields[1] == section_name:
            fileoff = int(fields[5], 16)
            sections[section_name] = (exec_load_address.base
                                      + fileoff
                                      - exec_load_address.offset)
      addresses[path] = (sections['.text'], sections)
    else:
      # Files that are not real DSOs have a fake .rodata section with the
      # complete contents of the file, to keep gdb happy.
      load_address = address_list[0]
      sections['.rodata'] = load_address.base
      addresses[path] = (sections['.rodata'], sections)
  return addresses


def _create_symbol_script(minidump_path, addresses):
  """Creates a gdb script to load symbols."""
  symbol_script_path = minidump_path + '.symbol_script.txt'
  if os.path.isfile(symbol_script_path):
    logging.debug('Reusing symbol script in %s', symbol_script_path)
    return symbol_script_path
  logging.info('Creating symbol script')
  symbol_script = []
  for symbol_path, v in sorted(addresses.items(), key=lambda x: x[1][0]):
    sections = v[1]
    extra_sections = ['-s %s 0x%x' % (name, addr) for name, addr in
                      sections.items() if name != '.text']
    symbol_script.append(
        'add-symbol-file %s 0x%x %s' % (symbol_path, v[0],
                                        ' '.join(extra_sections)))
  symbol_script.append('python exec(open("%s").read())' % os.path.join(
      os.path.abspath(os.path.dirname(os.path.realpath(__file__))),
      'gdb_pretty_printers.py'))

  with open(symbol_script_path, 'w') as f:
    f.write('\n'.join(symbol_script))

  return symbol_script_path


def _is_fuse_supported():
  try:
    import fuse as _
    return True
  except ImportError:
    logging.warning('FUSE is not supported, source code syncing is disabled. '
                    'You can install it with:')
    logging.warning('sudo apt install python3-pip && sudo pip3 install fusepy')
    return False


def _linkify_stack_trace(stack_trace, manifest):
  for line in stack_trace.strip().split('\n'):
    m = _STACK_TRACE_RE.match(line)
    if not m:
      continue
    frame, address, function, filename, line = m.groups()
    for project in manifest.projects:
      if not filename.startswith(project.path):
        continue
      revision = project.revision
      if not revision:
        revision = manifest.branch_name
      url = 'https://googleplex-android.git.corp.google.com/%s/+/%s/%s#%s' % (
          project.name, revision, os.path.relpath(filename, project.path),
          line)
      if address:
        print('%-3s %s in %s at %s' % (frame, address, function, url))
      else:
        print('%-3s %s at %s' % (frame, function, url))
      break


def main():
  parser = argparse.ArgumentParser(
      description=__doc__, epilog=EPILOG,
      formatter_class=argparse.RawTextHelpFormatter)
  parser.add_argument(
      '-v', '--verbose', action='count', default=0, help='Shows more messages.')
  parser.add_argument(
      '--no-fuse', action='store_true',
      help='Do not attempt to serve git files through FUSE.')
  parser.add_argument(
      '--manifest-path', default='.repo/manifests/default.xml',
      help='Path for the repo manifest file')
  subparsers = parser.add_subparsers(dest='command')
  subparsers.required = True

  parser_message = subparsers.add_parser('message',
                                         help='Prints the abort message')
  parser_message.add_argument(
      'minidump_path', metavar='<minidump>',
      help='Path to the minidump file, or the crash report URL')

  parser_print = subparsers.add_parser('print', help='Prints the stack trace')
  parser_print.add_argument(
      'minidump_path', metavar='<minidump>',
      help='Path to the minidump file, or the crash report URL')
  parser_print.add_argument('--commands', type=str, action='append', default=[],
                            help='commands to use. the default is "bt"')
  parser_print.add_argument('--linkify', action='store_true',
                            help='convert the filenames to hyperlinks')

  parser_debug = subparsers.add_parser('debug',
                                       help='Starts a cgdb session')
  parser_debug.add_argument(
      'minidump_path', metavar='<minidump>',
      help='Path to the minidump file, or the crash report URL')
  parser_debug.add_argument('--debugger', choices=('cgdb', 'gdb', 'gdb-tui'),
                            default='cgdb', help='Which debugger to use')

  args = parser.parse_args()

  logging.basicConfig(
      format='%(levelname)s: %(message)s',
      level=(logging.INFO, logging.DEBUG)[args.verbose])

  if lib.crash_dump_server.is_crash_server_url(args.minidump_path):
    crash_id = lib.crash_dump_server.get_report_id(args.minidump_path)
    args.minidump_path = lib.crash_dump_server.download_minidump(crash_id)
  minidump = lib.minidump.MinidumpFile(args.minidump_path)

  if args.command == 'message':
    print(_get_abort_message(args.minidump_path))
    return 0

  core_path = minidump.convert_to_core()
  if minidump.arc_build_id:
    artifact_fetcher = lib.build_artifact_fetcher.BuildArtifactFetcher(
        minidump.arch, 'user', minidump.arc_build_id)
    symbols = lib.symbol_file.RemoteSymbolFile(artifact_fetcher)
    symbols.unzip_symbols()
    target_files = lib.target_file.RemoteTargetFile(artifact_fetcher)
    target_files.unzip_target_files()

    manifest = lib.manifest_file.RemoteManifestFile(artifact_fetcher)
    manifest.download_manifest()
    manifest.parse_manifest()
  else:
    symbols = lib.symbol_file.LocalSymbolFile(minidump.arch)
    target_files = lib.target_file.LocalTargetFile(minidump.arch)

    manifest = lib.manifest_file.ManifestFile(args.manifest_path)
    manifest.parse_manifest()
  target_files.create_elf_files()
  load_addresses = _get_load_addresses(args.minidump_path)

  addresses = _get_addresses(load_addresses, symbols, target_files)
  symbol_script_path = _create_symbol_script(args.minidump_path, addresses)

  executable = None
  zygote_executable = None
  for k in addresses:
    if k.endswith(minidump.modules[0].name):
      executable = k
      break
    if k.endswith('app_process64'):
      zygote_executable = k
    elif k.endswith('app_process32') and zygote_executable is None:
      zygote_executable = k
  if not executable:
    # Java processes have either 'zygote' or their package name as process
    # name.  Fall back to the zygote executable for those.
    executable = zygote_executable

  params = []
  debugger = None
  if minidump.arch in ('x86', 'x86_64'):
    debugger = '/usr/bin/gdb'
  elif minidump.arch == 'arm':
    debugger = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                            '../../../../prebuilts/gdb/linux-x86/bin/gdb')
  else:
    assert False, 'Unsupported arch: %s' % minidump.arch

  if args.command == 'print':
    commands = ['set verbose off',
                'set logging file /dev/null',
                'set logging redirect on',
                'set logging on',
                ('source %s' % symbol_script_path),
                'set logging off']
    if args.commands:
      commands += args.commands
    else:
      commands.append('bt')
    commands.append('quit')
    params.extend([debugger, '--quiet', '--batch'])
    for command in commands:
      params.extend(['-ex', command])
    params.extend([executable, core_path])
    if args.linkify:
      _linkify_stack_trace(
          subprocess.check_output(params, universal_newlines=True), manifest)
    else:
      subprocess.check_call(params)
    return

  if args.command == 'debug':
    if args.debugger == 'gdb-tui':
      params.extend([debugger, '-tui'])
    elif args.debugger == 'gdb':
      params.extend([debugger])
    else:
      cgdb_path = '/usr/bin/cgdb'
      if not os.path.exists(cgdb_path):
        logging.error('cgdb is not installed. Try `sudo apt install -y cgdb`')
        sys.exit(1)
      params.extend([cgdb_path, '-d', debugger, '--'])
    params.extend(['-ex', ('source %s' % symbol_script_path)])

  if minidump.arc_build_id and _is_fuse_supported() and not args.no_fuse:
    with tempfile.TemporaryDirectory() as srcroot:
      # Redirecting both stdout/stderr to /dev/null because the debugger gets
      # all wonky if there's any additional unexpected output in the console.
      with subprocess.Popen(
          [os.path.join(os.path.dirname(os.path.realpath(__file__)),
                        'gitfs.py'), str(minidump.arc_build_id), srcroot],
          stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL) as p:
        params.extend(['-ex',
                       'directory %s:%s' % (srcroot,
                                            ':'.join(os.path.join(srcroot,
                                                                  dirname) for
                                                     dirname in
                                                     _JAVA_DIRECTORIES))])
        params.extend([executable, core_path])
        subprocess.check_call(params)
        p.terminate()
  else:
    params.extend(['-ex', 'directory %s' % ':'.join(_JAVA_DIRECTORIES)])
    params.extend([executable, core_path])
    subprocess.check_call(params)


if __name__ == '__main__':
  main()
