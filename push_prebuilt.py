#!/usr/bin/python3

# Copyright (C) 2016 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Downloads prebuilt artifacts from ab/ and pushes them into the device."""

import argparse
import logging
import os
import subprocess
import sys

_SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))

def main():
  logging.warning('This script is deprecated.  Please use push_to_device.py '
                  'instead (`%s --help` for detail).',
                  os.path.join(_SCRIPT_DIR, 'push_to_device.py'))

  if not os.environ['TARGET_PRODUCT']:
    sys.exit('No target product -- did you run lunch?')

  parser = argparse.ArgumentParser(description=__doc__)
  parser.add_argument(
      '--product', default=os.environ['TARGET_PRODUCT'])
  parser.add_argument(
      '--build-variant', default=os.environ['TARGET_BUILD_VARIANT'])
  parser.add_argument(
      'build_id', metavar='build-id', type=str,
      help='Build id as reported by ab/')
  parser.add_argument(
      'remote', type=str, help='Address of the device')
  args = parser.parse_args()

  subprocess.check_call([
      os.path.join(_SCRIPT_DIR, 'push_to_device.py'),
      '--use-prebuilt',
      '/'.join([args.product, args.build_variant, args.build_id]),
      args.remote])


if __name__ == '__main__':
  main()
