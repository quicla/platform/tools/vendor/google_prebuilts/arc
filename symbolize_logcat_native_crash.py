#!/usr/bin/python3

# Copyright (C) 2017 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Attempts to get symbol information and display the source code for a
stack trace recorded for a native crash recorded by logcat. """

from __future__ import print_function

import argparse
import logging
import os
import re
import sys

import lib.fingerprint
import lib.source_context_printer


EPILOG = """
Usage:
    %(prog)s path/to/logcat/output.log
    %(prog)s -  # To read logcat snippet from stdin
""".strip()

# The useful output from addr2line should look like:
#     <source path>:<line num><extra info>
BUILD_MACHINE_SOURCE_FILE_AND_LINE_RE = r'/proc/self/cwd/([^:]+?):(\d+)(.*)'
DEFAULT_CONTEXT_LINES = 5


class CrashInfo(object):
  """Holds details of a single backtrace extracted from the logcat output."""

  def __init__(self, backtrace=None, raw_lines=None):
    self.backtrace = backtrace or []
    self.raw_lines = raw_lines or []

  def __repr__(self):
    return (
        "CrashInfo: backtrace '%s' "
        "raw_lines '%s'") % (
            ('...[%d]...' % len(self.backtrace)) if self.backtrace else '',
            ('...[%d]...' % len(self.raw_lines)) if self.raw_lines else '')


class BacktraceEntry(object):
  """Holds an entry from  the backtrace extracted from the logcat output."""

  def __init__(self, i=None, pc=None, location=None):
    self.index = i
    self.pc = pc
    self.location = location

  def __repr__(self):
    return (
        "BacktraceEntry: index: %d pc: 0x%08x location: '%s'") % (
            self.index, self.pc, self.location)


def is_build_fingerprint(line):
  """Attempts to identify a line as containing a crash fingerprint value"""
  m = re.search(r"Build fingerprint: '([^']+)'$", line)
  return m is not None


def is_backtrace_start(line):
  """Attempts to identify a line that is the start of a crash backtrace"""
  m = re.search(r'backtrace:$', line)
  return m is not None


def get_backtrace_entry(line):
  """Attempts to identify a line with call stack information for a backtrace"""
  m = re.search(r'#(\d+) pc ([0-9a-f]+)  (.*)$', line)
  if not m:
    return None
  return BacktraceEntry(int(m.group(1)), int(m.group(2), 16), m.group(3))


class NativeLogcatCrashParser(object):
  """Parses logcat output looking for native crash dump lines."""

  def __init__(self):
    self._all_parsed_crashes = []
    self._parsed_crash = None
    self._index = None
    self._parse_line = None

  def _parse_backtrace_start(self, line):
    if not is_backtrace_start(line):
      return False
    logging.debug('Got backtrace start: %s', line)
    self._parse_line = self._parse_backtrace_entry
    self._index = 0
    return True

  def _parse_backtrace_entry(self, line):
    backtrace = get_backtrace_entry(line)
    if not backtrace:
      return False
    logging.debug('Got backtrace entry: %s', backtrace)
    self._parsed_crash.backtrace.append(backtrace)
    return True

  def _store_parsed_backtrace(self):
    if self._parsed_crash:
      logging.debug('Got crash: %s', self._parsed_crash)
      self._all_parsed_crashes.append(self._parsed_crash)
      self._parsed_crash = None

  def parse(self, content):
    """Parses the crashes out of |content|, returning a CrashInfo list."""

    self._all_parsed_crashes = []
    self._parsed_crash = None
    self._parse_line = None
    limit = 100

    for line in content.splitlines():
      # We always use the fingerprint line as an indicator that a new crash
      # is being started.
      if is_build_fingerprint(line):
        self._store_parsed_backtrace()
        self._parse_line = self._parse_backtrace_start
        limit = 100
        self._parsed_crash = CrashInfo()

      # Crash lines may be intermixed with other unrelated lines. We use a state
      # machine like approach with self._parsed_crash giving a function to use
      # to parse the next expected lines.
      if self._parse_line:
        limit -= 1
        if self._parse_line(line):
          self._parsed_crash.raw_lines.append(line)
          limit = 100
        elif limit < 0:
          self._store_parsed_backtrace()
          self._parse_line = None

    self._store_parsed_backtrace()

    return self._all_parsed_crashes


def parse_native_crashdump_logcat(content):
  """Parses out all the crashes reported via logcat output in |content|."""
  parser = NativeLogcatCrashParser()
  return parser.parse(content)


def print_backtrace_symbols(crash, symbol_start_path, source_context):
  """Print out as much information as is available for a crash."""
  print('Extracted logcat lines:')
  print('\n'.join(crash.raw_lines))
  print('')

  print('With added symbols and context:')
  for entry in crash.backtrace:
    print('%d 0x%08x %s' % (entry.index, entry.pc, entry.location))

    # Check to see if the location looks like a file path
    m = re.match(r'/([^ ]*)', entry.location)
    if not m:
      continue

    # Convert it to a path referencing the downloaded symbols
    symbol_source_path = os.path.join(symbol_start_path, m.group(1))

    # Attempt to print more information about this location
    source_context.print_symbols_and_context_for_address(
        symbol_source_path, '%x' % entry.pc)


def main():
  parser = argparse.ArgumentParser(
      description=__doc__, epilog=EPILOG,
      formatter_class=argparse.RawTextHelpFormatter)

  parser.add_argument(
      'logcat_output', type=argparse.FileType(),
      help='log file to parse, or - for stdin')

  parser.add_argument(
      '-C', '--context', dest='context_lines', default=DEFAULT_CONTEXT_LINES,
      help='lines of context to print')

  parser.add_argument(
      '-v', '--verbose', action='count', default=0,
      help='(repeatable) increase verbosity')

  args = parser.parse_args()
  logging.basicConfig(
      format='%(levelname)s: %(message)s',
      level=(logging.INFO, logging.DEBUG)[args.verbose])

  logcat_content = args.logcat_output.read()

  fingerprint = lib.fingerprint.get_fingerprint_from_log_content(logcat_content)
  if not fingerprint:
    sys.exit('Did not find a build fingerprint in the output.')

  logging.info(
      'Found ARC build %s variant %s abi %s', fingerprint.build_id,
      fingerprint.variant, fingerprint.abi)

  symbol_start_path = fingerprint.get_symbol_start_path()
  if fingerprint.is_local_build:
    if not os.path.exists(symbol_start_path):
      sys.exit('Please run this again from your root Android build directory. '
               'Unable to locate locally built symbols.')
    logging.info('Using local build symbols from %s', symbol_start_path)

  source_context = lib.source_context_printer.SourceContextPrinter(
      fingerprint.get_remote_manifest(), context_lines=args.context_lines)

  crashes = parse_native_crashdump_logcat(logcat_content)
  if not crashes:
    sys.exit('No native crashes found.')

  for i, crash in enumerate(crashes):
    if len(crashes) > 1:
      # We only worry about printing out a separator if there is more than one
      # crash.
      print('-' * 76)
      print('Crash', i)
    print_backtrace_symbols(crash, symbol_start_path, source_context)

if __name__ == '__main__':
  main()
