#!/bin/bash
#
# Copyright (C) 2018 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# This script helps to set up key-based authentication of adb. This can be
# useful if you don't have the device next by, and need to use adb after data
# wipe (otherwise, you need to touch the screen to accept the first connection).

set -e

if ! [[ -x "$(command -v adb)" ]]; then
  echo "adb not found in PATH"
  exit 1
fi

POSITIONAL=()
while [[ $# -gt 0 ]]; do
  case "$1" in
    --force-usb)
      FORCE_USB=1
      shift
      ;;
    *)
      POSITIONAL+=("$1")
      shift
      ;;
  esac
done

if [[ "${#POSITIONAL[@]}" -ne 1 ]]; then
  echo "Usage: $0 [--force-usb] hostname

Set up key based adb authentication on Chrome OS.

Options:
  --force-usb: force to turn on USB in settings."
  exit 1
fi

HOST="${POSITIONAL[0]}"
# This is the default key location that adb client uses.
KEY=~/.android/adbkey
PUB_KEY="${KEY}.pub"

if [[ ! -f "${KEY}" ]] || [[ ! -f "${PUB_KEY}" ]]; then
  echo "Generating key pair"
  adb keygen "${KEY}"
fi

if [[ "${FORCE_USB}" -eq "1" ]]; then
  echo "Enabling USB debugging"
  sys_usb_config="$(ssh \"${HOST}\" 'android-sh -c "getprop sys.usb.config"')"
  if [[ -z "${sys_usb_config}" ]]; then
    ssh "${HOST}" "android-sh -c 'setprop sys.usb.config usb'"
  elif [[ "${sys_usb_config}" != *usb* ]]; then
    ssh "${HOST}" \
    "android-sh -c 'setprop sys.usb.config \""${sys_usb_config}"\",usb'"
  fi
fi

echo "Injecting public key to adbd"
cat "${PUB_KEY}" | ssh "${HOST}" 'android-sh -c "cat > /data/misc/adb/adb_keys"'

ssh "${HOST}" "android-sh -c 'restorecon data/misc/adb/adb_keys 2> /dev/null\
  && stop adbd\
  && start adbd'"

echo "Stopping local adb server"
adb kill-server

echo "Connecting to ${HOST}:22"
adb connect "${HOST}:22"
